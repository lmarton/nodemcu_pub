
-- This function reads the statuses of all GPIO-ports
-- and calls the pubEvent to publish the results on MQTT
function updateStatus()
	for relay=1,maxRelays do
		relayStatus[relay] = gpio.read(relayPorts[relay])
	end
end

-- Initial setup of the realyPorts - all to a known value
function initRelays()
	for relay=1,maxRelays do
		gpio.mode(relayPorts[relay],gpio.OUTPUT)
		gpio.write(relayPorts[relay],relayStatus[relay])
	end
end

-- This function set the relay mode.
-- The relay is identified by the first character and the status by the second
function setRelay(message)
    -- debug
    -- print(message)

    relay = tonumber(string.sub(message,1,1))
    desiredState = tonumber(string.sub(message,2,2))

    if ( relay>=1 and relay<=maxRelays and (desiredState == 0 or desiredState == 1 ) ) then
        -- debug
        -- print("Relay: " .. relay .. "   -   Desired State: " .. desiredState)
        
        -- Set the relay
        gpio.write(relayPorts[relay],desiredState)
    else
	    print("Unknown command received")
    end
end


initRelays()

print("Relay control functions loaded...")
