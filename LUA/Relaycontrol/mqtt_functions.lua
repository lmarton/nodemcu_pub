
-- This function publishes a value to the MQTT "topicBase/status"
function pubEvent(pubValue)
	topicQueue = ''
    topicQueue = topicBase .. "/status"
	-- print('Publishing to ' .. topicQueue .. ": " .. message)	-- print a status message
	mqttBroker:publish(topicQueue, pubValue, 0, 1)	-- publish
end


function publishAll()
    updateStatus()
    for relay=1,maxRelays do
       message = relay .. relayStatus[relay]
       pubEvent(message)
    end
    -- debug 
    -- print('Published')
end

-- This calls the function to set the relay status and publishes the response
function processMessage(message)
    setRelay(message)
    pubEvent(message)
end

-- Reconnect to MQTT when we receive an "offline" message.
function reconn()
	print("Disconnected, reconnecting....")
	conn()
end


-- Establish a connection to the MQTT broker with the configured parameters.
function conn()
	print("Making connection to MQTT broker")
	mqttBroker:connect(mqttBrokerHost, mqttBrokerPort, 0, function(client) 
		                                                           print ("Broker connected")
		                                                           -- Subscribe to the base topic --
		                                                           mqttBroker:subscribe(topicBase, 0, function(client) print("Subscribed to CMD Topic: " .. topicBase) end ) 
		                                                  end, 
		                                                  function(client, reason) 
		                                                  	       print("failed reason: "..reason) 
		                                                  end 
		              )
end


-- Call this first! --
-- handleMQTT() instantiates the MQTT control object, sets up callbacks,
-- connects to the broker, and then uses the timer to send status data.

function handleMQTT()
	-- Instantiate a global MQTT client object
	print("Instantiating mqttBroker")
	mqttBroker = mqtt.Client(mqttClientID, mqttTimeOut, mqttUserID, mqttPass, 1)
     
	-- Set up the event callbacks
	print("Setting up callbacks")
	mqttBroker:on("connect", function(client) print ("Callback - connected") end)
	mqttBroker:on("offline", reconn)
	-- Set up Last Will and Testament (optional)
    -- Broker will publish a message with qos = 0, retain = 0, data = "offline"
    -- to topic "/lwt" if client don't send keepalive packet
    mqttBroker:lwt("/lwt", "Oups! Unable to send keepalive!", 0, 0)

    -- Set up the messge reception handling
    -- When a message is received on a subscribed channel we call the handler
	mqttBroker:on("message", function(client, topic, message )  processMessage(message) end )

	-- Connect to the Broker
    conn()

	-- Use the watchdog to call our sensor publication routine
	-- every dataInt seconds to send the sensor data to the 
	-- appropriate topic in MQTT.
	-- tmr.alarm(0, (dataInt * 1000), 1, publishAll)
    tmr.alarm(1, 30000 , 1, publishAll)
end

handleMQTT()

print("MQTT Funtionality Loaded...")
