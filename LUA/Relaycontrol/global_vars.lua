-- WIFI --
WIFI_SSID = "CHANGEME"
WIFI_PASS = "CHANGEME"
wifiTrys     = 1     -- Counter of trys to connect to wifi
NUMWIFITRYS  = 200    -- Maximum number of WIFI Testings while waiting for connection


-- MQTT --
mqttClientID = "RelayControl_1"	             -- an identifier for this device
mqttBrokerHost = "mqtt.server.changeme"	     -- target host (broker)
mqttBrokerPort = PORT_CHNAGEME	             -- target port (broker listening on)
mqttUserID = "relayboard1"		     -- account to use to log into the broker
mqttPass = "MQTT_PASS_CHANGEME" 	     -- broker account password
mqttTimeOut = 120		             -- connection timeout
dataInt = 30		    	             -- data transmission interval in seconds
topicBase = "home/groundfloor/relays"        -- the MQTT topic queue to use (Adjust according to your needs)


-- GPIO Relays --
maxRelays = 8                        -- the number of relays connected
-- A list of the PINS assigned to a relay
relayPorts = { 1, 2, 3, 4, 5, 6, 7, 8 }
relayStatus = { 0, 0, 0, 0, 0, 0, 0, 0 }
relayNames = { "Relay_1", "Relay_2", "Relay_3", "Relay_4", "Relay_5", "Relay_6", "Relay_7", "Relay_8" } 

-- Enabled Funtionality --
enableWebserver = 1
enableMQTT = 1


-- Feedback --
print("Global Variables Set...")
