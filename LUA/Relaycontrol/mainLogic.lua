function mainLogic()
     if ( enableMQTT == 1 ) then
     	print('Requested MQTT ...  Loading mqtt_functions.lua')
     	dofile('mqtt_functions.lua')
     end
     if ( enableWebserver == 1 ) then
     	print('Requested Webserver .. Loading web_functions.lua')
     	dofile('web_functions.lua')
     end
end

mainLogic()
