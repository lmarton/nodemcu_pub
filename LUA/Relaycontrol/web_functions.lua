
-- This Function explode the string at a divider
function explode(div,str)
    if (div=='') then return false end
    local pos,arr = 0,{}
    for st,sp in function() return string.find(str,div,pos,true) end do
        table.insert(arr,string.sub(str,pos,st-1))
        pos = sp + 1
    end
    table.insert(arr,string.sub(str,pos))
    return arr
end

-- Build and return a table of the http request data
function get_http_req (instr)
   local t = {}
   local first = nil
   local str_words

   for str in string.gmatch (instr, "([^\n]+)") do
   
      -- First line in the method and path
      if (first == nil) then
         first = 1
         str_words = explode(" ",str)
         t["METHOD"] = str_words[1]
         t["REQUEST"] = str_words[2]
      else -- Process and reamaining ":" fields
         if (str ~= nil) then
            str_words = explode(" ",str)
            if ( str_words[2] == nil ) then
               str_words = explode("=",str)
            end
            t[str_words[1]] = tostring(str_words[2])

            -- debug
            -- print(str_words[1] .. " - ") 
            -- print(str_words[2])
            
         end
      end
   end

   return t
end

-- This function return the intercative clickable page
function returnInteractive()
   local buf = "<H1>" .. mqttClientID .. "</H1>\n"
   buf = buf .. "<H2>Status</H2>\n"
   buf = buf .. "<form action=\"\" method=\"post\">\n"
   for relay=1,maxRelays do
      buf = buf .. relayNames[relay] .. " : "
      if (relayStatus[relay] == 1 ) then 
        buf = buf .. "<button name=\"Relay\" value=\"".. tostring(relay) .. "0\">OFF</button>[ON]<BR>\n"
      else
        buf = buf .. "[OFF]<button name=\"Relay\" value=\"".. tostring(relay) .. "1\">ON</button><BR>\n"
      end
   end 
   buf = buf .. "</form>\n"
   return buf
end

-- This function return the json response representing the Relay states
function returnJSON()
   local buf = "{\n"
   for relay=1,maxRelays-1 do
     buf = buf .. "  \"" .. relayNames[relay] .. "\" : " .. relayStatus[relay] .. ",\n"
   end 
   buf = buf .. "  \"" .. relayNames[maxRelays] .. "\" : " .. relayStatus[maxRelays] .. "\n}\n"
   return buf
end


srv=net.createServer(net.TCP)
srv:listen(80, function (conn)
                        conn:on("receive", function (conn, req_data)
                                                       local query_data = get_http_req (req_data)

                                                       -- debug
                                                       --print(req_data)


                                                       if (query_data["Relay:"] ~= nil) then
                                                          setRelay(query_data["Relay:"])
                                                          updateStatus()
                                                       end
                                                       if (query_data["Relay"] ~= nil) then
                                                          setRelay(query_data["Relay"])
                                                          updateStatus()
                                                       end
                                                      
                                                       local response
                                                       if (query_data["REQUEST"] == "/") then
                                                          response = returnInteractive()
                                                       elseif (query_data["REQUEST"] == "/json") then
                                                          response = returnJSON()
                                                       else
                                                          response = "UNKNOWN REQUEST"
                                                       end

                                                       -- print(response)

                                                       conn:send(response)
                                              end
                                 )
                        conn:on("sent", function() conn:close() collectgarbage() end )
               end
          )

print('Webserver enabled. Access at http://' .. wifi.sta.getip() )
